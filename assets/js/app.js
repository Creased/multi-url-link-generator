$(document).ready(function() {
	function successRequest(data) {
		$("#output").html(data);
	}
	
	$("#mainForm").submit(function(event) {
		event.preventDefault();
	
		$("#errors").html("");
	
		var form = $(this);
	
		jQuery.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: form.serialize(),
			async: true,
			resetForm: false,
			success: successRequest
		}, "json");
	
		return false;
	});
});
